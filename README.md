<!--
This file is part of SoSy-Lab Java-Project Template,
a collection of common files and build definitions for Java projects:
https://gitlab.com/sosy-lab/software/java-project-template

SPDX-FileCopyrightText: 2018-2020 Dirk Beyer <https://www.sosy-lab.org>

SPDX-License-Identifier: Apache-2.0
-->

# SoSy-Lab Project Template for Java Projects

[![Apache 2.0 License](https://img.shields.io/badge/license-Apache--2-brightgreen.svg?style=flat)](https://www.apache.org/licenses/LICENSE-2.0)

This project collects common files for all our Java projects,
mostly definitions for the build system and configuration files for tools.

Content:
- `base/`: Files which are useful for new projects,
  but which typically have local modifications in the actual projects.
- `files/`: Files that should be present in our projects
  (except if a specific feature is not required in a project)
- `Docker/`: Docker files for containers that can be used
  as base for project-specific containers for tests on GitLab CI
- `recommended/`: Files that should be present in our projects
  (except if a specific feature is not required in a project),
  but where some projects have local modifications
  that should not be overwritten.

The script `syncTo.sh` can be used to apply changes in this repository
to another project.
It outputs a diff with recommended changes if there are differences
between a project's files and the files in `recommended/`.

Most of the files in this project template
are licensed under the [Apache 2.0 License](https://www.apache.org/licenses/LICENSE-2.0)
and have copyright by [Dirk Beyer](https://www.sosy-lab.org/people/beyer/),
with a few exceptions.
