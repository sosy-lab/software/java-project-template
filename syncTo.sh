#!/bin/bash

# This file is part of SoSy-Lab Java-Project Template,
# a collection of common files and build definitions for Java projects:
# https://gitlab.com/sosy-lab/software/java-project-template
#
# SPDX-FileCopyrightText: 2018-2020 Dirk Beyer <https://www.sosy-lab.org>
#
# SPDX-License-Identifier: Apache-2.0

set -euo pipefail
IFS=$'\n\t'
shopt -s globstar
shopt -s dotglob

DIR="${1:-}"
PROJECT="${2:-}"
DESCRIPTION="${3:-}"
URL="${4:-}"

URL=$(echo "$URL" | sed -e 's/[\/&]/\\&/g')

if [ -z "$DIR" ] || [ \! -d "$DIR" ] || [ -z "$PROJECT" ]; then
  echo "Call script as:"
  echo "$0 path/to/project/directory 'NAME OF PROJECT' 'DESCRIPTION' 'URL'"
  echo "Example: $0 ../cpachecker CPAchecker 'a tool for configurable software verification' 'https://cpachecker.sosy-lab.org'"
  exit 1
fi

DIR="$(realpath "$DIR")"

cd "$(dirname ${0})/files"
find . -mindepth 1 -type d -exec mkdir -p "$DIR/{}" \;
for file in ./**/*.*; do
  if [ -f "$file" ]; then
    sed \
      -e "s/<<PROJECT>>/$PROJECT/g" \
      -e "s/<<DESCRIPTION>>/$DESCRIPTION/g" \
      -e "s/<<URL>>/$URL/g" \
      "$file" > "$DIR/$file"
  fi
done
cd ..

cd "$(dirname ${0})/recommended"
for file in ./**/*.*; do
  if [ -f "$DIR/$file" ]; then
    diff -u \
      "$DIR/$file" \
      <(sed \
        -e "s/<<PROJECT>>/$PROJECT/g" \
        -e "s/<<DESCRIPTION>>/$DESCRIPTION/g" \
        -e "s/<<URL>>/$URL/g" \
        "$file") \
      || true
  fi
done
cd ..
